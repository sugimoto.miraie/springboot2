package com.example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthController {

    @Autowired
    UserService userService;
    
//    @GetMapping("/signup")
//    public String signup(Model model) {
//        model.addAttribute("signupForm", new SignupForm());
//        return "signup";
//    }
//
//    @PostMapping("/signup")
//    public String signupPost(Model model, @Valid SignupForm signupForm,
//    		BindingResult bindingResult, HttpServletRequest request) {
//    	
//        if (bindingResult.hasErrors()) {
//            return "signup";
//        }
//
//        try {
//            userService.registerUser(signupForm.getUsername(),
//            		signupForm.getPassword(), signupForm.getMailAddress());
//        } catch (DataIntegrityViolationException e) {
//            model.addAttribute("signupError", true);
//            return "signup";
//        }
//
//        try {
//            request.login(signupForm.getUsername(), signupForm.getPassword());
//        } catch (ServletException e) {
//            e.printStackTrace();
//        }
//
//        return "redirect:/messages";
//    }
//    

    @RequestMapping("/")
    public String inde() {
        return "redirect:/top";
    }
    
    @GetMapping("/login")
    public String index() {
        return "index";
    }

//    @PostMapping("/login")
//    public String loginPost() {
//        return "redirect:/login-error";
//    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "index";
    }
    

    @GetMapping("/top")
    public ModelAndView topGet(ModelAndView mav){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
 		mav.addObject("user",(User)auth.getPrincipal());
    	mav.addObject("msg","ようこそ" +((User)auth.getPrincipal()).getUsername() + "さん！");
//    	mav.addObject("msg","ようこそ" + "さん！");
    	mav.setViewName("top");
    	return mav;
    }
    
    @RequestMapping(value="/top",method=RequestMethod.POST)
    public ModelAndView send(@RequestParam("username")String str,ModelAndView mav) {
    	mav.addObject("msg","ようこそ"+ str +"さん！");
    	mav.setViewName("top");
    	return mav;
    }
}