package com.example;

import java.util.InputMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements UserDetailsService {

    private static final Integer Integer = null;

	@Autowired
    private UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
   
    public User loadUserByUsername1(String username) throws UsernameNotFoundException {
        if (username == null || "".equals(username)) {
            throw new UsernameNotFoundException("Username is empty");
        }

        User user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        return user;
    }
    
    
    public User loadUserByEmployeeNumber(String employeeNumber) throws InputMismatchException { 
       if (employeeNumber == null || "".equals(employeeNumber)) {
            throw new InputMismatchException("EmployeeNumber is empty");
        }

        User user = repository.findByEmployeeNumber(employeeNumber);
        if (user == null) {
            throw new InputMismatchException("User not found: " + employeeNumber);
        }

        return user;
    }

    @Transactional
    public void registerAdmin( String employeeNumber, String username, String password, String mailAddress) {
        User user = new User( employeeNumber, username, passwordEncoder.encode(password), mailAddress);
        user.setAdmin(true);
        repository.save(user);
    }

    @Transactional
    public void registerUser(String employeeNumber,String username, String password, String mailAddress) {
        User user = new User( employeeNumber, username, passwordEncoder.encode(password), mailAddress);
        repository.save(user);
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}